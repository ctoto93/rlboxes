import collections
import numpy as np
import tensorflow as tf
import pyswarms as ps
import gin

options = {'c1': 0.02, 'c2': 0.1, 'w':0.7}

@gin.configurable
class PSOPlanner:

    def __init__(self, env, world_model, critic, particles=100, iterations=25, iteration_interval=None, length=5 , gamma=0.9):
        self.env = env
        self.world_model = world_model
        self.critic = critic
        self.particles = particles
        self.iterations = iterations
        self.iteration_interval = iteration_interval
        self.length = length
        self.gamma = gamma
        self.history = []
        self.current_episodes = 0

        # attributes used for plotting
        self.plans = []
        self.plans_imagined_states = []
        self.plans_value = []

    def new_optimizer(self):
        optimizer = ps.single.GlobalBestPSO(
                n_particles=self.particles,
                dimensions=self.dimensions(),
                options=options,
                bounds=self.bounds())

        return optimizer

    def bounds(self):
        low = self.env.action_space.low
        high = self.env.action_space.high

        min_bound = np.tile(low, self.length)
        max_bound = np.tile(high, self.length)
        return (min_bound, max_bound)

    def action_length(self):
        return self.env.action_space.shape[0]

    def dimensions(self):
        return self.length * self.action_length()

    def schedule_iterations(self):
        return 1 + (self.current_episodes // self.iteration_interval)

    def generate(self, state):
        self.state = state
        self.optimizer = self.new_optimizer()
        iters = self.schedule_iterations() if self.iteration_interval else self.iterations
        best_reward, best_plan = self.optimizer.optimize(self.pso_cost_func,
                                    iters=iters,
                                    verbose=False)
        self.history.append({
            "state": self.env.state,
            "cost_history": self.optimizer.cost_history,
            "plan": best_plan
        })

        self.plans = self.optimizer.swarm.position
        self.plans_value = self.optimizer.swarm.current_cost

        return best_plan.reshape(self.length, -1)

    def pso_cost_func(self, x):
        value = np.zeros((self.particles, 1))
        state = np.tile(self.state, (self.particles, 1))

        predicted_states = []

        for i in range(self.length):
            from_idx = i * self.action_length()
            to_idx = (i+1) * self.action_length()
            action = x[:,from_idx:to_idx].reshape(self.particles, self.action_length())
            state_action = np.concatenate((state, action), axis=1)
            state = self.world_model(state_action)

            value += self.critic(state)
            predicted_states.append(state)

        cost = value.numpy().reshape(-1) * -1 # min the cost instead of max rewards since pyswarms can only minimize

        self.plans_imagined_states = np.array(predicted_states)

        return np.squeeze(cost)
