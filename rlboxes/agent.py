from abc import ABC, abstractmethod

class Agent(ABC):

    @abstractmethod
    def run(self):
        pass

    @abstractmethod
    def train_step(self):
        pass

    @abstractmethod
    def behavior_policy(self, state):
        pass

    @abstractmethod
    def save(self, dir):
        pass

    @abstractmethod
    def set_current_episode(n):
        pass
