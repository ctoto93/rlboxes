from __future__ import absolute_import

from absl import app
from absl import flags
from pathlib import Path
from tqdm import tqdm

from rlboxes import CACLA, PurePlanning, Evaluator, BestRandomPlanner, PSOPlanner
from rlboxes.constants import DEFAULT_MODELS_DIR
from tensorflow.keras import models
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import SGD, RMSprop, Adam
from time import time


import tensorflow as tf
import gym
import gin
import os
import numpy as np
from rlboxes.utils.plotter.rbf_grid_world import visualize_critic_with_plans


CMD_LOAD = "load"
CMD_NEW = "new"
CMD_GEN_FIGS = "figures"

FLAGS = flags.FLAGS
flags.DEFINE_string("name", "default", "models name")
flags.DEFINE_integer("repeat", 1, "train how many times")

# Required flag.
flags.mark_flag_as_required("name")

@gin.configurable
def new_model(dir, critic_lr=1e-3, actor_lr=1e-4):
    env = gym.make("ContinuousGridWorld-v0", radial_basis_fn=True, cumulative=True)

    n_obs = env.observation_space.shape[0]
    n_action = env.action_space.shape[0]

    critic = models.Sequential()
    critic.add(Dense(64, activation='relu', input_shape=(n_obs,)))
    critic.add(Dense(64, activation='relu'))
    critic.add(Dense(32, activation='relu'))
    critic.add(Dense(1))
    critic.compile(loss='mean_squared_error', optimizer=Adam(learning_rate=critic_lr))

    wm = load_world_model()
    planner = PSOPlanner(env, wm, critic)
    agent = PurePlanning(env, planner, critic)

    evaluator = Evaluator(env, agent, dir=dir)
    evaluator.train_and_evaluate()
    visualize_critic_with_plans(env, agent, dir)

@gin.configurable
def load_world_model(path="world_models/default.h5"):
    wm = models.load_model(path)
    return wm

def load_model(dir):
    env = gym.make("ContinuousGridWorld-v0")
    env._max_episode_steps = 100000

    dir = "{}/{}".format(DEFAULT_MODELS_DIR, FLAGS.name)
    actor = tf.keras.models.load_model("{}/actor_model.h5".format(dir))
    critic = tf.keras.models.load_model("{}/critic_model.h5".format(dir))

    agent = CACLA(env, actor, critic)
    agent.run()


def main(argv):
    command = argv[1]

    gin.parse_config_file(os.path.join(os.path.dirname(__file__), 'config.gin'))

    if command == CMD_NEW:
        for i in range(FLAGS.repeat):
            print(f"TRAIN AGENT {i}")
            dir = "{}/{}_{}".format(DEFAULT_MODELS_DIR, FLAGS.name, i)
            path = Path(dir).mkdir(parents=True, exist_ok=True)
            new_model(dir)
    elif command == CMD_LOAD:
        load_model(dir)
    elif command == CMD_GEN_FIGS:
        env = gym.make("ContinuousGridWorld-v0")
        wm = load_world_model()
        # smell code but change later
        planner = PSOPlanner(env, wm, None)
        agent = PurePlanning(env, planner, None)
        visualize_critic_with_plans(env, agent, dir)

if __name__ == '__main__':
  app.run(main)
