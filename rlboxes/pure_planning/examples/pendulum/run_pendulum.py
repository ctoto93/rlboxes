from __future__ import absolute_import

from absl import app
from absl import flags
from pathlib import Path
from tqdm import tqdm

from rlboxes import CACLA, PurePlanning, Evaluator, BestRandomPlanner, PSOPlanner
from rlboxes.constants import DEFAULT_MODELS_DIR
from tensorflow.keras import models
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import SGD, RMSprop, Adam
from time import time
from rlboxes.envs.sparse_pendulum import SparsePendulumRBFEnv, REWARD_MODE_BINARY
from rlboxes.utils.plotter.common import plot_train_loss_and_cumulative_rewards
from rlboxes.utils.plotter.rbf_pendulum import visualize_pendulum


import tensorflow as tf
import gym
import gin
import os
import numpy as np


CMD_LOAD = "load"
CMD_NEW = "new"
CMD_GEN_FIGS = "figures"

FLAGS = flags.FLAGS
flags.DEFINE_string("name", "default", "models name")

# Required flag.
flags.mark_flag_as_required("name")

@gin.configurable
def new_model(dir, critic_lr=1e-3, actor_lr=1e-4):
    env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])
    eval_env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])

    n_obs = env.observation_space.shape[0]
    n_action = env.action_space.shape[0]

    critic = models.Sequential()
    critic.add(Dense(1024, activation='relu', input_shape=(n_obs,)))
    critic.add(Dense(512, activation='relu'))
    critic.add(Dense(256, activation='relu'))
    critic.add(Dense(1))
    critic.compile(loss='mean_squared_error', optimizer=Adam(learning_rate=critic_lr))

    wm = load_world_model()
    planner = PSOPlanner(env, wm, critic)
    agent = PurePlanning(env, planner, critic)

    evaluator = Evaluator(eval_env, agent, dir=dir)
    evaluator.train_and_evaluate()

@gin.configurable
def load_world_model(path="world_models/default.h5"):
    wm = models.load_model(path)
    return wm

def load_model(dir):
    env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])
    env._max_episode_steps = 100000

    dir = "{}/{}".format(DEFAULT_MODELS_DIR, FLAGS.name)
    actor = tf.keras.models.load_model("{}/actor_model.h5".format(dir))
    critic = tf.keras.models.load_model("{}/critic_model.h5".format(dir))

    agent = CACLA(env, actor, critic)
    agent.run()


def main(argv):
    command = argv[1]
    dir = "{}/{}".format(DEFAULT_MODELS_DIR, FLAGS.name)
    gin.parse_config_file(os.path.join(os.path.dirname(__file__), 'config.gin'))

    if command == CMD_NEW:
        path = Path(dir).mkdir(parents=True, exist_ok=True)
        new_model(dir)
    elif command == CMD_LOAD:
        load_model(dir)
    elif command == CMD_GEN_FIGS:
        env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])
        wm = load_world_model()
        # smell code but change later

        models_path = Path(dir)

        sorted_critic_files = sorted(models_path.glob("critic_model_eps_*.h5"), key=os.path.getmtime)
        critic = models.load_model(sorted_critic_files[-1])
        planner = PSOPlanner(env, wm, critic)
        agent = PurePlanning(env, planner, critic)
        plot_train_loss_and_cumulative_rewards(dir)
        visualize_pendulum(agent, dir)

if __name__ == '__main__':
  app.run(main)
