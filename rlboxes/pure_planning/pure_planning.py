from rlboxes import Agent, Runner, CACLA
from tensorflow import keras
from tensorflow.keras import optimizers, losses
from tensorflow.keras.layers import Input, Dense, GaussianNoise
from rlboxes import Runner
import tensorflow as tf
import numpy as np
import gin
import time

USE_PLANNING = 0
USE_MODEL_FREE = 1

@gin.configurable
class PurePlanning(Agent):

    def __init__(self,
                env,
                planner,
                critic,
                n_steps=5,
                gamma=0.9,
                epsilon_greedy=0.2):

        self.env = env
        self.planner = planner
        self.critic = critic
        self.n_steps = n_steps
        self.gamma = gamma
        self.runner = Runner(env, self, n_steps, gamma)
        self.epsilon_greedy = epsilon_greedy

    def actor(self, state):
        action = self.behavior_policy(state)
        return tf.convert_to_tensor(action, dtype=tf.float32)

    def train_step(self):
        states, _, rewards , values, next_state_values = self.runner.run()
        v_targets = rewards + self.gamma * next_state_values
        critic_loss = self.critic.fit(states, v_targets, verbose=0).history["loss"][0]

        return None, critic_loss

    def run(self):
        state = self.env.reset()
        state = np.array([state])
        done = False

        while not done:
            self.env.render()
            action = self.actor(state)
            state, reward, done, _ = self.env.step(action)
            state = state.reshape(1,-1)
            time.sleep(0.025)
            print(f'reward: {reward} done: {done}')

    def call(self, state):
        return self.actor(state)

    def behavior_policy(self, state):
        if not self.should_explore():
            plan = self.planner.generate(state)
            return plan[:1].reshape(1,-1)

        return self.env.action_space.sample().reshape(1,-1)

    def should_explore(self):
        return np.random.uniform(0,1) < self.epsilon_greedy

    def set_current_episode(self, n):
        self.planner.current_episode = n

    def save(self, dir="./logs"):
        self.critic.save("{}/critic_model_eps_{}.h5".format(dir, self.planner.current_episode))
