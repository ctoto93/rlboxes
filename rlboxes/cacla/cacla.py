from rlboxes import Agent, Runner
from tensorflow import keras
from tensorflow.keras import optimizers, losses
from tensorflow.keras.layers import Input, Dense, GaussianNoise
from rlboxes import Runner
import tensorflow as tf
import numpy as np
import gin
import time

@gin.configurable
class CACLA(Agent):

    def __init__(self,
                env,
                actor,
                critic,
                n_steps=5,
                gamma=0.9,
                exploration_noise_std=0.1,
                target_noise_clip=0.5):

        self.env = env
        self.actor = actor # seq model
        self.critic = critic # seq model
        self.exploration_noise_std = exploration_noise_std
        self.gamma = gamma
        self.target_noise_clip = target_noise_clip
        self.runner = Runner(env, self, n_steps, gamma)

    def train_step(self):
        self.runner.run()
        state, action, next_state, reward, _ = self.runner.replay_buffer.sample()
        value = self.critic(state).numpy()
        next_state_value = self.critic(next_state).numpy()
        v_target = reward + self.gamma * next_state_value

        critic_loss = self.critic.fit(state, v_target, verbose=0).history["loss"][0]

        index = (next_state_value > value).reshape(-1) # only update action next state leads to good values

        actor_loss = None
        if index.any():
            actor_loss = self.actor.fit(state[index], action[index], verbose=0).history["loss"][0]

        return actor_loss, critic_loss

    def run(self):
        state = self.env.reset()
        state = np.array([state])
        done = False

        while True:
            self.env.render()
            action = self.actor(state)
            state, reward, done, _ = self.env.step(action)
            state = state.reshape(1,-1)
            time.sleep(0.025)
            print(f'reward: {reward} done: {done}')

    def call(self, state):
        return self.actor(state)

    def behavior_policy(self, state):
        action = self.actor(state)
        noise = np.random.normal(scale=self.exploration_noise_std, size=(action.shape))
        action += noise
        action = np.clip(action, self.env.action_space.low, self.env.action_space.high)

        return action

    def set_current_episode(self, n):
        self.current_episode = n

    def save(self, dir="./logs"):
        self.actor.save("{}/actor_model_eps_{}.h5".format(dir, self.current_episode))
        self.critic.save("{}/critic_model_eps_{}.h5".format(dir, self.current_episode))
