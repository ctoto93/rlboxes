from __future__ import absolute_import

from absl import app
from absl import flags
from pathlib import Path
from tqdm import tqdm

from rlboxes import CACLA, Evaluator
from rlboxes.constants import DEFAULT_MODELS_DIR
from tensorflow.keras import models
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import SGD, RMSprop, Adam
from time import time
from PIL import Image

import tensorflow as tf
import gym
import gin
import os
import re
import imageio
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt



CMD_LOAD = "load"
CMD_NEW = "new"

FLAGS = flags.FLAGS
flags.DEFINE_string("name", "default", "models name")
flags.DEFINE_integer("repeat", 1, "train how many times")

# Required flag.
flags.mark_flag_as_required("name")

@gin.configurable
def new_model(dir, critic_lr=1e-3, actor_lr=1e-4):
    env = gym.make("ContinuousGridWorld-v0", radial_basis_fn=True, cumulative=True)

    n_obs = env.observation_space.shape[0]
    n_action = env.action_space.shape[0]

    actor = models.Sequential()
    actor.add(Dense(64, activation='tanh', input_shape=(n_obs,)))
    actor.add(Dense(64, activation='tanh'))
    actor.add(Dense(32, activation='tanh'))
    actor.add(Dense(n_action))
    actor.compile(loss='mean_squared_error', optimizer=Adam(learning_rate=actor_lr))

    critic = models.Sequential()
    critic.add(Dense(64, activation='relu', input_shape=(n_obs,)))
    critic.add(Dense(64, activation='relu'))
    critic.add(Dense(32, activation='relu'))
    critic.add(Dense(1))
    critic.compile(loss='mean_squared_error', optimizer=Adam(learning_rate=critic_lr))

    model = CACLA(env, actor, critic)

    evaluator = Evaluator(env, model, dir=dir)
    evaluator.train_and_evaluate()
    create_actor_critic_gifs(env, dir)

def load_model(dir):
    env = gym.make("ContinuousGridWorld-v0")
    env._max_episode_steps = 100000

    dir = "{}/{}".format(DEFAULT_MODELS_DIR, FLAGS.name)
    actor = tf.keras.models.load_model("{}/actor_model.h5".format(dir))
    critic = tf.keras.models.load_model("{}/critic_model.h5".format(dir))

    agent = CACLA(env, actor, critic)
    agent.run()


def main(argv):
    command = argv[1]
    gin.parse_config_file(os.path.join(os.path.dirname(__file__), 'config.gin'))

    if command == CMD_NEW:
        for i in range(FLAGS.repeat):
            print(f"TRAIN AGENT {i}")
            dir = "{}/{}_{}".format(DEFAULT_MODELS_DIR, FLAGS.name, i)
            path = Path(dir).mkdir(parents=True, exist_ok=True)
            new_model(dir)
    elif command == CMD_LOAD:
        load_model(dir)

def generate_critic_heatmap(env, critic, episode=0):
    steps = np.arange(0, 1.0, 0.05)
    data = [[0 for x in range(len(steps))] for y in range(len(steps))]
    for i, y in enumerate(steps):
        for j, x in enumerate(steps):
            obs = np.array([[x, y]])
            transformed_obs = env.transformer.transform(obs)
            value = critic(transformed_obs)
            data[i][j] = value.numpy()[0][0]


    df = pd.DataFrame(data, index=steps, columns=steps)

    plt.figure(figsize=(20,10))
    vmax = 1.5
    plt.pcolor(df, vmin=0, vmax=vmax)
    plt.ylabel("Y")
    plt.yticks(np.arange(0.5, len(df.index), 1), df.index.values.round(2))
    plt.xlabel("X")
    plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns.values.round(2))
    plt.title("Critic Episode {}".format(episode), fontsize=16)
    plt.colorbar()

    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()

    pil_image = Image.frombytes('RGB', canvas.get_width_height(),
                 canvas.tostring_rgb())
    plt.close()

    return pil_image

def generate_actor_fields(env, actor, episode=0):
    steps = np.arange(0, 1.0, 0.05)
    xs, ys = np.meshgrid(steps, steps)
    u = [[None for x in range(len(steps))] for y in range(len(steps))]
    v = [[None for x in range(len(steps))] for y in range(len(steps))]

    for i, y in enumerate(steps):
        for j, x in enumerate(steps):
            obs = np.array([[x, y]])
            transformed_obs = env.transformer.transform(obs)
            action = actor(transformed_obs)
            ax, ay = action.numpy()[0]

            u[i][j] = ax
            v[i][j] = ay

    u = np.array(u)
    v = np.array(v)

    fig, ax = plt.subplots(1, figsize=(10,10))
    ax.quiver(xs, ys, u, v)
    fig.suptitle("Actor episode {}".format(episode), fontsize=24)

    canvas = fig.canvas
    canvas.draw()

    pil_image = Image.frombytes('RGB', canvas.get_width_height(),
                 canvas.tostring_rgb())

    return pil_image


def create_gif(imgs, fp_out, duration=200):
    img, *imgs = imgs
    img.save(fp=fp_out, format='GIF', append_images=imgs,
         save_all=True, duration=duration, loop=0, quality=20)

def create_actor_critic_gifs(env, dir):
    models_path = Path(dir)
    critic_heatmap_path = models_path / 'figs' / 'critic_heatmap.gif'
    actor_vector_fields_path = models_path / 'figs' / 'actor_vector_fields.gif'

    sorted_critic_files = sorted(models_path.glob("critic_model_eps_*.h5"), key=os.path.getmtime)
    sorted_actor_files = sorted(models_path.glob("actor_model_eps_*.h5"), key=os.path.getmtime)

    vector_fields_imgs = []
    heatmap_imgs = []

    print("### GENERATE VECTOR FIELDS AND HEATMAP GIFS ###")
    for actor_file, critic_file in tqdm(zip(sorted_actor_files, sorted_critic_files), total=len(sorted_critic_files)):
        eps = re.search(r"eps_(\d+).h5$", str(actor_file)).group(1)

        actor = models.load_model(actor_file)
        critic = models.load_model(critic_file)

        heatmap_imgs.append(generate_critic_heatmap(env, critic, eps))
        vector_fields_imgs.append(generate_actor_fields(env, actor, eps))

    create_gif(heatmap_imgs, critic_heatmap_path)
    create_gif(vector_fields_imgs, actor_vector_fields_path)

if __name__ == '__main__':
  app.run(main)
