from __future__ import absolute_import

from absl import app
from absl import flags
from pathlib import Path

from rlboxes import CACLA, Evaluator
from rlboxes.constants import DEFAULT_MODELS_DIR
from rlboxes.utils.plotter.rbf_pendulum import visualize_pendulum
from rlboxes.utils.plotter.common import plot_train_loss_and_cumulative_rewards
from tensorflow.keras import models
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import SGD, RMSprop
import tensorflow as tf
import gym
import gin
from time import time
import os
from rlboxes.envs.sparse_pendulum import SparsePendulumRBFEnv, REWARD_MODE_BINARY

CMD_LOAD = "load"
CMD_NEW = "new"
CMD_GEN_FIGS = "figures"

FLAGS = flags.FLAGS
flags.DEFINE_string("name", "default", "models name")

# Required flag.
flags.mark_flag_as_required("name")

@gin.configurable
def new_model(dir, critic_lr=1e-3, actor_lr=1e-4):
    env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])
    env._max_episode_steps = 100000

    eval_env = env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])
    env._max_episode_steps = 100000

    n_obs = env.observation_space.shape[0]
    n_action = env.action_space.shape[0]

    actor = models.Sequential()
    actor.add(Dense(1024, activation='relu', input_shape=(n_obs,)))
    actor.add(Dense(512, activation='relu'))
    actor.add(Dense(256, activation='relu'))
    actor.add(Dense(n_action))
    actor.compile(loss='mean_squared_error', optimizer=RMSprop(learning_rate=actor_lr))

    critic = models.Sequential()
    critic.add(Dense(1024, activation='relu', input_shape=(n_obs,)))
    critic.add(Dense(512, activation='relu'))
    critic.add(Dense(256, activation='relu'))
    critic.add(Dense(1))
    critic.compile(loss='mean_squared_error', optimizer=RMSprop(learning_rate=critic_lr))

    agent = CACLA(env, actor, critic)

    evaluator = Evaluator(eval_env, agent, dir=dir)
    evaluator.train_and_evaluate()
    visualize_pendulum(agent, dir)

def load_model(dir):
    env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])
    env._max_episode_steps = 100000

    models_dir = "models/cacla_pendulum_100k"
    critic = tf.keras.models.load_model("{}/critic_model.h5".format(models_dir))
    actor = tf.keras.models.load_model("{}/actor_model.h5".format(models_dir))

    agent = CACLA(env, actor, critic)
    agent.run()


def main(argv):
    command = argv[1]
    dir = "{}/{}".format(DEFAULT_MODELS_DIR, FLAGS.name)
    gin.parse_config_file(os.path.join(os.path.dirname(__file__), 'config.gin'))

    if command == CMD_NEW:
        path = Path("{}/{}".format(dir, "critic_heatmap")).mkdir(parents=True, exist_ok=True)
        new_model(dir)
    elif command == CMD_LOAD:
        load_model(dir)
    elif command == CMD_GEN_FIGS:
        env = SparsePendulumRBFEnv(8.0, 2.0, 2.0, 1.0, 5, REWARD_MODE_BINARY, n_rbf=[5, 5, 17])
        agent = CACLA(env, None, None)
        plot_train_loss_and_cumulative_rewards(dir)
        visualize_pendulum(agent, dir)

if __name__ == '__main__':
  app.run(main)
