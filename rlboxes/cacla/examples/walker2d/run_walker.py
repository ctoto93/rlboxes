from __future__ import absolute_import

from absl import app
from absl import flags
from pathlib import Path

from rlboxes import CACLA, Evaluator
from rlboxes.constants import DEFAULT_MODELS_DIR
from tensorflow.keras import models
from tensorflow.keras.layers import Dense, Input
from tensorflow.keras.optimizers import SGD, RMSprop
import tensorflow as tf
import gym
import gin
from time import time
import os

CMD_LOAD = "load"
CMD_NEW = "new"

FLAGS = flags.FLAGS
flags.DEFINE_string("name", "default", "models name")

# Required flag.
flags.mark_flag_as_required("name")

@gin.configurable
def new_model(dir, critic_lr=1e-3, actor_lr=1e-4):
    env = gym.make("BipedalWalker-v3")
    env._max_episode_steps = 100000

    eval_env = gym.make("BipedalWalker-v3")
    env._max_episode_steps = 100000

    n_obs = env.observation_space.shape[0]
    n_action = env.action_space.shape[0]

    actor = models.Sequential()
    actor.add(Dense(64, activation='tanh', input_shape=(n_obs,)))
    actor.add(Dense(64, activation='tanh'))
    actor.add(Dense(32, activation='tanh'))
    actor.add(Dense(n_action))
    actor.compile(loss='mean_squared_error', optimizer=RMSprop(learning_rate=actor_lr))

    critic = models.Sequential()
    critic.add(Dense(64, activation='tanh', input_shape=(n_obs,)))
    critic.add(Dense(64, activation='tanh'))
    critic.add(Dense(32, activation='tanh'))
    critic.add(Dense(1))
    critic.compile(loss='mean_squared_error', optimizer=RMSprop(learning_rate=critic_lr))

    model = CACLA(env, actor, critic)

    evaluator = Evaluator(eval_env, model, dir=dir)
    evaluator.train_and_evaluate()

def load_model(dir):
    env = gym.make("BipedalWalker-v3")
    env._max_episode_steps = 100000

    models_dir = "models/default"
    critic = tf.keras.models.load_model("{}/critic_model.h5".format(models_dir))
    actor = tf.keras.models.load_model("{}/actor_model.h5".format(models_dir))

    agent = CACLA(env, actor, critic)
    agent.run()


def main(argv):
    command = argv[1]
    dir = "{}/{}".format(DEFAULT_MODELS_DIR, FLAGS.name)
    gin.parse_config_file(os.path.join(os.path.dirname(__file__), 'config.gin'))

    if command == CMD_NEW:
        path = Path("{}/{}".format(dir, "critic_heatmap")).mkdir(parents=True, exist_ok=True)
        new_model(dir)
    elif command == CMD_LOAD:
        load_model(dir)

if __name__ == '__main__':
  app.run(main)
