from rlboxes.agent import Agent
from rlboxes.replay_buffer import ReplayBuffer
from rlboxes.runner import Runner
from rlboxes.evaluator import Evaluator
from rlboxes.cacla.cacla import CACLA
from rlboxes.pip.pip import PlanningIntegratedPolicy
from rlboxes.best_random_planner import BestRandomPlanner
from rlboxes.pso_planner import PSOPlanner
from rlboxes.pure_planning.pure_planning import PurePlanning
from gym.envs.registration import register

register(
    id='ContinuousGridWorld-v0',
    entry_point='rlboxes.envs:ContinuousGridWorldEnv',
    kwargs={
         'width': 1,
         'height': 1,
         'max_step': 0.1,
         'goal_x': 0.45,
         'goal_y': 0.45,
         'goal_side': 0.1,
         'radial_basis_fn': True,
         'x_dim': 10,
         'y_dim': 10,
         'cumulative': False
    }

)
