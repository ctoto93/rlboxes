import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("Agg")
import re
import os
from tqdm import tqdm
from tensorflow.keras import models
from PIL import Image


from matplotlib.pyplot import cm
colors = cm.get_cmap("tab20").colors

from pathlib import Path
from rlboxes.utils.gif import create_gif

def inverse_transform(env, x):
    if hasattr(env, 'transformer'):
        x = env.transformer.inverse_transform(x)
    return np.clip(x, 0, 1)

def to_plot_xy(x):
    return x * 20 + 0.5

def evaluate_critic(env, critic):
    steps = np.arange(0, 1.01, 0.05)
    data = [[0 for x in range(len(steps))] for y in range(len(steps))]
    for i, y in enumerate(steps):
        for j, x in enumerate(steps):
            obs = np.array([[x, y]])
            if hasattr(env, 'transformer'):
                obs = env.transformer.transform(obs)

            value = critic(obs)
            data[i][j] = value.numpy()[0][0]

    return pd.DataFrame(data, index=steps, columns=steps)

def get_filename_eps(filename):
    return int(re.search(r'^.*_eps_(\d+).h5$', str(filename)).group(1))

def visualize_critic_with_plans(env, agent, dir):
    models_path = Path(dir)
    critic_heatmap_path = models_path / 'figs' / 'critic_heatmap.gif'
    actor_vector_fields_path = models_path / 'figs' / 'actor_vector_fields.gif'

    sorted_critic_files = sorted(models_path.glob("critic_model_eps_*.h5"), key=get_filename_eps)[:1]

    vmax = evaluate_critic(env, models.load_model(sorted_critic_files[-1])).to_numpy().max() # use last critic vmax
    vector_fields_imgs = []
    heatmap_imgs = []

    print("### GENERATE VECTOR FIELDS AND HEATMAP GIFS ###")
    for critic_file in tqdm(sorted_critic_files):
        eps = re.search(r"eps_(\d+).h5$", str(critic_file)).group(1)

        #this also smells
        critic = models.load_model(critic_file)
        agent.critic = critic
        agent.planner.critic = critic
        df_critic_value = evaluate_critic(env, agent.critic)

        heatmap_imgs.append(generate_critic_heatmap(df_critic_value, agent, vmax, eps))

    create_gif(heatmap_imgs, critic_heatmap_path)

def actual_plan_states(initial_state, planner):
    actual_states = np.tile(initial_state, (planner.length, planner.particles,1))
    for i in range(planner.length - 1):
        actual_states[i+1] = np.clip(actual_states[i] + planner.plans[:,(i*2):(i+1)*2], 0,1)
    return actual_states

def generate_critic_heatmap(df, agent, vmax, episode=0):
    env = agent.env
    planner = agent.planner

    plt.figure(figsize=(20,10))
    plt.pcolor(df, vmin=0, vmax=vmax)
    plt.ylabel("Y")
    plt.yticks(np.arange(0.5, len(df.index), 1), df.index.values.round(2))
    plt.xlabel("X")
    plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns.values.round(2))
    plt.title("Critic Episode {}".format(episode), fontsize=16)
    plt.colorbar()

    # generate plan visualization
    state = env.reset()

    x_init, y_init = to_plot_xy(inverse_transform(env, state.reshape(1,-1))[0])
    plt.scatter(x_init, y_init, s=200, c='r')

    planner.generate(state)
    plans = planner.plans
    plans_imagined_states = planner.plans_imagined_states
    plans_value = planner.plans_value
    actual_states = actual_plan_states(env.state, planner) * 20 + 0.5

    transform_predicted_states = []
    for i in range(planner.length):
        transform_predicted_states.append(to_plot_xy(inverse_transform(env, plans_imagined_states[i])))
    transform_predicted_states = np.asanyarray(transform_predicted_states)

    arrows = []
    labels = []

    best_plan_ids = np.argsort(plans_value)[::-1][:3]
    for i, plan_id in enumerate(best_plan_ids):
        x = x_init
        y = y_init
        act_x, act_y = actual_states[0][i]

        for j in range(planner.length):
            if j == planner.length - 1:
                plt.scatter(x, y, s=100, marker='x', c=[colors[i]])
                plt.scatter(act_x2, act_y2, s=100, marker='x', c=[colors[i]])
                break

            x2, y2 = transform_predicted_states[j][plan_id]
            act_x2, act_y2 = actual_states[j][plan_id]

            dx = x2 - x
            dy = y2 - y

            act_dx = act_x2 - act_x
            act_dy = act_y2 - act_y

            arrow = plt.arrow(x,y,dx,dy, head_width=0.15, color=colors[i % 20], linestyle="-")
            act_arrow = plt.arrow(act_x,act_y,act_dx,act_dy, head_width=0.15, color=colors[i % 20], linestyle="dotted")

            if j == 0:
                arrows.append(arrow)
                labels.append("imagined plan {}".format(i))

                arrows.append(act_arrow)
                labels.append("actual plan {}".format(i))

            x = x2
            y = y2

            act_x = act_x2
            act_y = act_y2



    plt.legend(arrows, labels, loc='upper center', bbox_to_anchor=(0.5, 1.1), fancybox=True, shadow=True, ncol=10)

    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()

    pil_image = Image.frombytes('RGB', canvas.get_width_height(),
                 canvas.tostring_rgb())
    plt.close()
    return pil_image
