import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("Agg")
import re
import os
from tqdm import tqdm
from tensorflow.keras import models
from PIL import Image

from gym.envs.classic_control.pendulum import angle_normalize

from matplotlib.pyplot import cm
colors = cm.get_cmap("tab20").colors

from pathlib import Path
from rlboxes.utils import rbf
from rlboxes.utils.gif import create_gif

def scale_to_plot_coordinate(x, x_min, x_max, n):
    return (((x - x_min) / (x_max - x_min)) * (n - 1)) + 0.5

def evaluate_critic(env, critic):
    angles = np.linspace(-180, 180, 17)
    thetas = np.radians(angles)
    thetadots = np.linspace(-8, 8, 17)

    data = [[0 for thetadot in range(len(thetadots))] for theta in range(len(thetas))]
    for i, theta in enumerate(thetas):
        for j, thetadot in enumerate(thetadots):

            obs = np.array([[np.cos(theta), np.sin(theta), thetadot]])
            if env.transformer:
                obs = env.transformer.transform(obs)
            value = critic(obs)
            data[i][j] = value.numpy()[0][0]


    return pd.DataFrame(data, index=angles, columns=thetadots)

def plot_heatmap(df, vmax, episode=0):
    plt.figure(figsize=(20,10))
    plt.pcolor(df, vmin=0, vmax=vmax)
    plt.ylabel("Y")
    plt.yticks(np.arange(0.5, len(df.index), 1), df.index.values.round(2))
    plt.xlabel("X")
    plt.xticks(np.arange(0.5, len(df.columns), 1), df.columns.values.round(2))
    plt.title("Critic Episode {}".format(episode), fontsize=16)
    plt.colorbar()

def plot_plan(agent):
    env = agent.env
    planner = agent.planner

    state = env.reset()

    x, y, thetadot = env.transformer.inverse_transform(state.reshape(1,-1))[0]
    angle = np.degrees(angle_normalize(np.math.atan2(y, x)))
    thetadot = np.clip(-8,8,thetadot)

    angle = scale_to_plot_coordinate(angle, -180, 180, 17)
    thetadot = scale_to_plot_coordinate(thetadot, -8, 8, 17)

    x_init, y_init = np.array([thetadot, angle])
    plt.scatter(x_init, y_init, s=200, c='r')

    planner.generate(state)
    plans = planner.plans
    plans_imagined_states = planner.plans_imagined_states
    plans_value = planner.plans_value

    transform_predicted_states = []
    for i in range(planner.length):
        inverse_transformed_states = env.transformer.inverse_transform(plans_imagined_states[i])
        angle = np.degrees(
            angle_normalize(
                np.arctan2(inverse_transformed_states[:,1], inverse_transformed_states[:,0])
        ))
        thetadot = np.clip(-8, 8, inverse_transformed_states[:,2])

        angle = scale_to_plot_coordinate(angle, -180, 180, 17)
        thetadot = scale_to_plot_coordinate(thetadot, -8, 8, 17)

        transform_predicted_states.append(np.array([thetadot, angle]))
    transform_predicted_states = np.asanyarray(transform_predicted_states).reshape(planner.length, planner.particles, -1)
    arrows = []
    labels = []

    best_plan_ids = np.argsort(plans_value)[::-1][:3]
    for i, plan_id in enumerate(best_plan_ids):
        x = x_init
        y = y_init

        for j in range(planner.length):
            if j == planner.length - 1:
                plt.scatter(x, y, s=100, marker='x', c=[colors[i]])
                break

            x2, y2 = transform_predicted_states[j][plan_id]

            dx = x2 - x
            dy = y2 - y

            arrow = plt.arrow(x,y,dx,dy, head_width=0.15, color=colors[i % 20])

            if j == 0:
                arrows.append(arrow)
                labels.append("plan rank {}".format(i))

            x = x2
            y = y2



    plt.legend(arrows, labels, loc='upper center', bbox_to_anchor=(0.5, 1.1), fancybox=True, shadow=True, ncol=10)


def visualize_pendulum(agent, dir):
    env = agent.env
    models_path = Path(dir)
    critic_heatmap_path = models_path / 'figs' / 'critic_heatmap.gif'
    actor_vector_fields_path = models_path / 'figs' / 'actor_vector_fields.gif'

    sorted_critic_files = sorted(models_path.glob("critic_model_eps_*.h5"), key=os.path.getmtime)
    sorted_actor_files = sorted(models_path.glob("actor_model_eps_*.h5"), key=os.path.getmtime)

    vmax = evaluate_critic(env, models.load_model(sorted_critic_files[-1])).to_numpy().max()
    vector_fields_imgs = []
    heatmap_imgs = []

    print("### GENERATE VECTOR FIELDS AND HEATMAP GIFS ###")
    for critic_file in tqdm(sorted_critic_files, total=len(sorted_critic_files)):
        eps = re.search(r"eps_(\d+).h5$", str(critic_file)).group(1)

        critic = models.load_model(critic_file)
        df_critic_value = evaluate_critic(env, critic)

        plot_heatmap(df_critic_value, vmax, eps)
        if hasattr(agent, 'planner'):
            plot_plan(agent)

        canvas = plt.get_current_fig_manager().canvas
        canvas.draw()

        pil_image = Image.frombytes('RGB', canvas.get_width_height(),
                     canvas.tostring_rgb())

        plt.close()
        heatmap_imgs.append(pil_image)

    create_gif(heatmap_imgs, critic_heatmap_path)
