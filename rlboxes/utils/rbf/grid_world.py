from tensorflow.keras import models
import numpy as np

model = models.load_model("world_models/continuous_grid_world/inverse_rbf.h5")

def inverse_transform(x):
    predicted = model.predict(x)
    return np.clip(predicted, 0, 1)
