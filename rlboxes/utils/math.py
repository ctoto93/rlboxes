import numpy as np
def scale(space, action):
    """
    Rescale the action from [low, high] to [-1, 1]
    (no need for symmetric action space)
    :param space: (gym.spaces.box.Box)
    :param action: (np.ndarray)
    :return: (np.ndarray)
    """
    low, high = space.low, space.high
    return 2.0 * ((action - low) / (high - low)) - 1.0


def unscale(space, scaled_action):
    """
    Rescale the action from [-1, 1] to [low, high]
    (no need for symmetric action space)
    :param space: (gym.spaces.box.Box)
    :param action: (np.ndarray)
    :return: (np.ndarray)
    """
    low, high = space.low, space.high
    return low + (0.5 * (scaled_action + 1.0) * (high - low))
