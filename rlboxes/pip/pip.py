from rlboxes import Agent, Runner, CACLA
from tensorflow import keras
from tensorflow.keras import optimizers, losses
from tensorflow.keras.layers import Input, Dense, GaussianNoise
from rlboxes import Runner
import tensorflow as tf
import numpy as np
import gin
import time

USE_PLANNING = 0
USE_MODEL_FREE = 1

@gin.configurable
class PlanningIntegratedPolicy(CACLA):

    def __init__(self,
                env,
                planner,
                actor,
                critic,
                n_steps=5,
                gamma=0.9,
                exploration_noise_std=0.1,
                target_noise_clip=0.5):

        super().__init__(env, actor, critic, n_steps, gamma, exploration_noise_std, target_noise_clip)

        self.planner = planner
        mf_steps = n_steps - planner.length
        self.runner = Runner(env, self, mf_steps, gamma)
        self.mode = USE_PLANNING

    def train_step(self):
        self.mode = USE_PLANNING
        return super().train_step()

    def run(self):
        state = self.env.reset()
        state = np.array([state])
        done = False

        while not done:
            self.env.render()
            action = self.actor(state)
            state, reward, done, _ = self.env.step(action)
            state = state.reshape(1,-1)
            time.sleep(0.025)
            print(f'reward: {reward} done: {done}')

    def call(self, state):
        return self.actor(state)

    def behavior_policy(self, state):
        if self.mode == USE_PLANNING:
            plan = self.planner.generate(state)
            for action in plan:
                state, _, _, _ = self.env.step(action)

            self.mode == USE_MODEL_FREE
            state = state.reshape(1,-1)

        return super().behavior_policy(state)
