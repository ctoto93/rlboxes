import pandas as pd
import numpy as np
import gin

from tqdm import tqdm
from rlboxes.utils.plotter.common import plot_train_loss_and_cumulative_rewards

import matplotlib.pyplot as plt
import seaborn as sns

@gin.configurable
class Evaluator:

    def __init__(self, env, agent, n_steps=5, episodes=1000, interval=50, n_eval=3, dir="./agents/default", fig_window_smooth=10000):
        self.env = env
        self.agent = agent
        self.n_steps = n_steps
        self.episodes = episodes
        self.interval = interval
        self.n_eval = n_eval
        self.dir = dir
        self.fig_window_smooth = fig_window_smooth

    def train_and_evaluate(self):
        df_eval = pd.DataFrame(columns =  ["episode", "cum_rewards", "eval"])
        df_train = pd.DataFrame(columns =  ["episode", "actor_loss", "critic_loss"])

        print("### START TRAINING ###")
        bar = tqdm(total=self.episodes)
        for episode in range(self.episodes):
            self.agent.set_current_episode(episode)
            actor_loss, critic_loss = self.agent.train_step()
            df_train = df_train.append({
                "episode": episode,
                "actor_loss": actor_loss,
                "critic_loss": critic_loss,
                "train_cum_reward": self.agent.runner.cum_reward
            }, ignore_index=True)

            if episode % self.interval == 0:
                for eval in range(self.n_eval):
                    cum_rewards = self.evaluate_current_policy()
                    df_eval = df_eval.append({"episode": episode, "cum_rewards": cum_rewards, "eval":eval}, ignore_index=True)
                self.agent.save(self.dir)
                bar.update(self.interval)

        bar.close()

        print("### GENERATE FIGURES ###")
        df_train.to_pickle("{}/df_train.pkl".format(self.dir))
        df_eval.to_pickle("{}/df_eval.pkl".format(self.dir))
        plot_train_loss_and_cumulative_rewards(self.dir)
        self.agent.save(self.dir)

    def evaluate_current_policy(self):
        state = self.env.reset()
        state = np.array([state])
        done = False
        cum_rewards = 0

        for _ in range(self.n_steps):
            action = self.agent.actor(state).numpy().reshape(-1)
            state, reward, done, _ = self.env.step(action)
            state = state.reshape(1,-1)
            cum_rewards += reward
            if done:
                break

        return cum_rewards
