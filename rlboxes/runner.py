import numpy as np
import gym
from rlboxes import ReplayBuffer

class Runner:
    def __init__(self, env, model, n_steps=5, gamma=0.99):
        """
        A runner to learn the policy of an environment for an a2c model
        :param env: (Gym environment) The environment to learn from
        :param model: (Model) The model to learn
        :param n_steps: (int) The number of steps to run for each environment
        :param gamma: (float) Discount factor
        """
        self.env = env
        self.model = model
        self.n_steps = n_steps
        self.gamma = gamma

        state_dim = env.observation_space.shape[0]
        action_dim = env.action_space.shape[0]
        self.replay_buffer = ReplayBuffer(state_dim, action_dim)


    def run(self):
        """
        Run a learning step of the model
        :return: ([float], [float], [float])
                 states, actions, rewards
        """
        self.cum_reward = 0
        states = []
        actions = []
        rewards = []

        state = self.env.reset()

        for _ in range(self.n_steps):
            action = self.model.behavior_policy(state.reshape(1,-1))[0]

            next_state, reward, done, _ = self.env.step(action)
            self.replay_buffer.add(state, action, next_state, reward, done)
            state = next_state
            self.cum_reward += reward
            if done:
                break
