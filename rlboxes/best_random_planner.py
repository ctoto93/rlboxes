import numpy as np
import gin

@gin.configurable
class BestRandomPlanner:

    def __init__(self, env, world_model, critic, candidates=100, length=5, gamma=0.9):
        self.env = env
        self.world_model = world_model
        self.length = length
        self.gamma = gamma
        self.critic = critic
        self.candidates = candidates

        # attributes used for plotting
        self.plans = []
        self.plans_imagined_states = []
        self.plans_value = []

    def generate_random_plans(self):
        plans = []
        for _ in range(self.candidates):
            plan = [self.env.action_space.sample() for _ in range(self.length)]
            plans.append(plan)

        self.plans = np.array(plans)

    def get_best_plan(self, state):
        self.plans_value, self.plans_imagined_states = self.evaluate_plans(state)
        idx_best = np.argmax(self.plans_value)

        return self.plans[idx_best]

    def evaluate_plans(self, state):
        value = np.zeros((self.candidates, 1))
        state = np.tile(state, (self.candidates, 1))

        predicted_states = []

        for i in range(self.length):
            action = self.plans[:,i]
            state_action = np.concatenate((state, action), axis=1)
            state = self.world_model(state_action)
            value += self.critic(state)
            predicted_states.append(state.numpy())

        value = value.numpy().reshape(-1)
        predicted_states = np.array(predicted_states)

        return value, predicted_states

    def generate(self, state):
        self.generate_random_plans()
        return self.get_best_plan(state)
