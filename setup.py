import setuptools

setuptools.setup(name='rlboxes',
    version='0.0.1',
    author="Christoper Glenn Wulur",
    packages=[package for package in setuptools.find_packages() if package.startswith('rlboxes')],
    zip_safe=False,
    author_email="christoper.glennwu@gmail.com",
    description="Reinforcement Learning Toolbox",
    install_requires=['gym', 'tensorflow']#And any other dependencies required
)
